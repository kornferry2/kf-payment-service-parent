package com.pdinh.payment.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hps.integrator.entities.HpsTransactionDetails;
import com.hps.integrator.entities.credit.HpsCardHolder;
import com.hps.integrator.entities.credit.HpsCharge;
import com.hps.integrator.infrastructure.HpsException;
import com.hps.integrator.services.HpsCreditService;
import com.hps.integrator.services.HpsServicesConfig;
import com.pdinh.payment.util.MailService;

/**
 * Servlet implementation class HPSAPIServlet
 */
@WebServlet(urlPatterns = { "/HPSAPIServlet" }, initParams = {}, loadOnStartup = 1)
public class HPSAPIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(HPSAPIServlet.class);
	
	private String redirectURI;

	@Resource(mappedName = "java:global/application/payment-service/config_properties")
	private Properties configProperties;

	@EJB
	private MailService mailService;

	// private final MailService mailService = new MailService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HPSAPIServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		String incomingURL = configProperties.getProperty("paymentwebBaseUrl");

		response.setHeader("Access-Control-Allow-Origin", "incomingURL");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "*");
		response.setHeader("Access-Control-Max-Age", "300");

		log.info("PROCESSING PAYMENT SERVLET DOPOST");
		// String publicKey = "pkapi_cert_3VvH4wUJcbTWyn4tOd"; // kyamry
		String secretKey = configProperties.getProperty("paymentServerSecretKey");// "skapi_cert_MdUxAQCbRRsAiuvrho9Y3IHJLD4mIwy54AIMPlxTZg";
																					// kyamry
		String errorMsg = "";

		// Authentication Methods
		HpsServicesConfig config = new HpsServicesConfig();
		config.setSecretAPIKey(secretKey);
		config.setDeveloperId("002914");
		config.setVersionNumber("1810");
		// response.setContentType("application/json");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		// Gson gson = new Gson();

		// HpsCreditCard card = new HpsCreditCard();

		// String news = request.getParameter("card_cvc");

		// card.setCvv(Integer.parseInt(request.getParameter("card_cvc")));
		// card.setExpMonth(Integer.parseInt(request.getParameter("exp_month")));
		// card.setExpYear(Integer.parseInt(request.getParameter("exp_year")));
		// card.setNumber(request.getParameter("card_number"));

		HpsCardHolder cardHolder = new HpsCardHolder();
		// ZIP REQUIRED!
		cardHolder.getAddress().setZip(request.getParameter("bill_zip"));
		// ADDRESS STUFF IS OPTIONAL-but we've added it in case the biz changes
		// their mind as they like to do...
		cardHolder.getAddress().setAddress(
				request.getParameter("bill_address_1") + " " + request.getParameter("bill_address_2") + " "
						+ request.getParameter("bill_address_3"));
		cardHolder.getAddress().setCity(request.getParameter("bill_city"));
		cardHolder.getAddress().setState(request.getParameter("bill_state"));
		cardHolder.getAddress().setCountry(request.getParameter("bill_country"));

		String invoices = "";
		invoices = request.getParameter("invoice1");

		String invchk = "";
		for (int i = 2; i <= 15; i++) {
			invchk = "invoice" + i;
			if (request.getParameterMap().containsKey(invchk)) {
				invoices = invoices + ", " + request.getParameter(invchk);
			}
		}

		HpsTransactionDetails details = new HpsTransactionDetails(request.getParameter("client_name"), invoices, "");

		HpsCreditService service = null;
		try {
			service = new HpsCreditService(config);
		} catch (HpsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Handling Errors
		String tok = request.getParameter("token_value");
		BigDecimal amount = new BigDecimal(request.getParameter("amount"));
		try {
			redirectURI = "complete.xhtml?faces-redirect=true";
			HpsCharge charge = null;
			charge = service.charge(amount, "usd", tok, cardHolder, false, false, null, details);
			// response.getWriter().write(gson.toJson(charge));
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();

			String message = "<h3>Heartland Payment Systems Transaction"
					+ "</h3><table><tr><td>Transaction ID </td><td>"
					+ charge.getTransactionID()
					+ "</td><tr>"
					+ "<tr><td>Client Name:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("client_name"))
					+ "</td></tr>"
					+ "<tr><td>Server Date/time:</td><td>"
					+ dateFormat.format(date)
					+ "</td></tr>"
					// + "<tr><td>Customer Number</td><td>"
					// +
					// StringEscapeUtils.escapeHtml4(request.getParameter("customer_number"))
					// + "</td></tr>"
					+ "<tr><td>Invoice number(s):</td><td>"
					+ StringEscapeUtils.escapeHtml4(invoices)
					+ "</td></tr>"
					+ "<tr><td>Amount (USD):</td><td>"
					+ request.getParameter("amount")
					+ "</td></tr>"
					+ "<tr><td>Card Type:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("card_type"))
					+ "</td></tr>"
					+ "<tr><td>Name on card:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("name_on_card"))
					+ "</td></tr>"
					+ "<tr><td>Billing Address 1:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("bill_address_1"))
					+ "</td></tr>"
					+ "<tr><td>Billing Address 2:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("bill_address_2"))
					+ "</td></tr>"
					+ "<tr><td>Billing Address 3:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("bill_address_3"))
					+ "</td></tr>"
					+ "<tr><td>Billing Country:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("bill_country"))
					+ "</td></tr>"
					+ "<tr><td>Billing State/Province/Region:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("bill_state"))
					+ "</td></tr>"
					+ "<tr><td>Billing City/Town:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("bill_city"))
					+ "</td></tr>"
					+ "<tr><td>Billing Zip/Postal Code:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("bill_zip"))
					+ "</td></tr><tr><td><hr/></td><td><hr/></td></tr>"
					+ "<tr><td>Contact Name:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("contact_name"))
					+ "</td></tr>"
					+ "<tr><td>Phone:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("phone"))
					+ "</td></tr>"
					+ "<tr><td>Email:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("email"))
					+ "</td></tr>"
					+ "<tr><td>Address 1:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("address_1"))
					+ "</td></tr>"
					+ "<tr><td>Address 2:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("address_2"))
					+ "</td></tr>"
					+ "<tr><td>Address 3:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("address_3"))
					+ "</td></tr>"
					+ "<tr><td>Country:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("country"))
					+ "</td></tr>"
					+ "<tr><td>State/Province/Region:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("state"))
					+ "</td></tr>"
					+ "<tr><td>City/Town:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("city"))
					+ "</td></tr>"
					+ "<tr><td>Zip/Postal Code:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("zip"))
					+ "</td></tr>"
					+ "<tr><td>Other Comments:</td><td>"
					+ StringEscapeUtils.escapeHtml4(request.getParameter("other_comments"))
					+ "</td></tr></table><p>Do not reply to this email.</p>";
			try {
				mailService.sendMailMessage("Invoice Payment - KF Card Pay", "doNotReplyCCpayments@kornferry.com",
						configProperties.getProperty("paymentServerEmail"), message);
			} catch (Exception em) {
				log.debug("Email failed to send: Invoice Payment - PDINH paymentweb , doNotReplyCCpayments@kornferry.com, "
								+ configProperties.getProperty("paymentServerEmail") + " " + message);
				log.debug("Stacktrace is: " + em.getStackTrace());
				 response.setStatus(response.SC_INTERNAL_SERVER_ERROR, "Email failed to send: Invoice Payment - PDINH paymentweb , doNotReplyCCpayments@kornferry.com, "
							+ configProperties.getProperty("paymentServerEmail") + " " + message);

			}
	
			response.setStatus(response.SC_OK);
		
		} catch (HpsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();


			response.setStatus(response.SC_BAD_GATEWAY, "The request failed due to: "
                    + e.getMessage());
		}

	}
}
