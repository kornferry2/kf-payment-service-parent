package com.pdinh.payment.util;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@Stateless
@LocalBean
public class MailService {
	@Resource(name = "java:jboss/mail/pdinh")
	private Session mailSession;

	/**
	 * Default constructor.
	 */
	public MailService() {
	}

	public Boolean sendMailMessage(String subject, String fromAddress, String toAddress, String message) {
		MimeMessage msg = new MimeMessage(mailSession);
		Multipart multiPart = new MimeMultipart("alternative");

		try {

			MimeBodyPart text = new MimeBodyPart();
			MimeBodyPart html = new MimeBodyPart();

			msg.setSubject(subject, "utf-8");
			if (!fromAddress.equals("")) {
				msg.setFrom(new InternetAddress(fromAddress));
			}
			msg.setRecipient(RecipientType.TO, new InternetAddress(toAddress));

			text.setText(TextUtils.getPlainText(message), "utf-8");
			multiPart.addBodyPart(text);

			html.setContent(message, "text/html; charset=utf-8");
			multiPart.addBodyPart(html);

			msg.setContent(multiPart);

			Transport.send(msg);

			System.out.println("Sent message to {} successfully." + toAddress);

			return true;
		} catch (MessagingException me) {
			System.out.println("There was an error sending the email. {}" + me.getMessage());
			System.out.println("message destination: " + toAddress);
			System.out.println("message body: " + message);
			System.out.println("---------------END FAILED EMAIL------------ ");

			return false;
		} catch (Exception e) {
			System.out.println("There was an error sending the email. {}" + e.getMessage());
			System.out.println("message destination: " + toAddress);
			System.out.println("message body: " + message);
			System.out.println("---------------END FAILED EMAIL------------ ");
			return false;
		}
	}

}
